! Copyright (C) 2022 Quan Zillan.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays hashtables help.markup help.syntax images kernel
math strings urls ;
IN: e621

HELP: <e621.net/posts.json>
{ $values
    { "url-tuple" e621.net/posts.json }
}
{ $description "Constructs a default e621.net/posts.json request" } ;

HELP: >search-url
{ $values
    { "tuple" e621.net/posts.json }
    { "url" string }
}
{ $description "converts a e621.net/posts.json tuple into a url string" } ;

HELP: apply-blacklist
{ $values
    { "tags" string }
    { "tags+blacklist" string }
}
{ $description "Takes a string and appends the default blacklist to it." } ;

HELP: default-blacklist
{ $var-description "Holds the default blacklist. All tags here must start with - (dash)." } ;

HELP: default-score
{ $var-description "Default minimum score for a post to be included." } ;

HELP: e6
{ $values
    { "tags" string }
    { "image" image }
}
{ $description "fetches a random image from e6" } ;

HELP: e6*
{ $values
    { "posts.json" e621.net/posts.json }
    { "images" null }
}
{ $description "" } ;

HELP: e621.net/posts.json
{ $class-description "Represents an e621 posts.json request." } ;

HELP: fetch-multiple-sample-images
{ $values
    { "posts" array }
    { "images" array }
}
{ $description "Downloads multiple images in multiple post and returns an array of images." } ;

HELP: fetch-sample-image
{ $values
    { "post" hashtable }
    { "image" image }
}
{ $description "Downloads the image at the URL in the post object and returns it as an image." } ;

HELP: format-url
{ $values
    { "tags" string } { "score" integer } { "order" string } { "limit" integer }
    { "url" url }
}
{ $description "Constructs a search URL from it's components." } ;

HELP: get-random-e621-post
{ $values
    { "tags" string }
    { "post" hashtable }
}
{ $description "Get a random post matching the given set of tags." } ;

HELP: post-url
{ $values
    { "value" string }
}
{ $description "Holds a format string for e621 post lookup request" } ;

HELP: search-e621-posts
{ $values
    { "tuple" e621.net/posts.json }
    { "posts" array }
}
{ $description "Fetches all post that match the provided querry." } ;

HELP: tags>url-param
{ $values
    { "tags" string }
    { "search-tags" string }
}
{ $description "Formats a space seperate string into a + seperated string" } ;

HELP: wait-for-rate-limit
{ $description "Waits 1 second to avoid triggering e621's rate limit." } ;

ARTICLE: "e621" "e621"
{ $vocab-link "e621" }
;

ABOUT: "e621"
