! Copyright (C) 2022 Quan Zillan.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays continuations hashtables help.markup help.syntax
images.viewer kernel strings ui.gadgets ui.gadgets.borders
ui.gadgets.labels ui.gadgets.packs ;
IN: e621.ui

HELP: <filled-pile>
{ $values
    { "filled-pile" pack }
}
{ $description "" } ;

HELP: <no-results-label>
{ $values
    { "tags" string }
    { "label" label }
}
{ $description "" } ;

HELP: <post-description-label>
{ $values
    { "post" hashtable }
    { "post-description-label" label }
}
{ $description "" } ;

HELP: <post-gadget>
{ $values
    { "post" hashtable }
    { "gadgets" array }
}
{ $description "" } ;

HELP: <post-id-label>
{ $values
    { "post" hashtable }
    { "post-id-label" label }
}
{ $description "" } ;

HELP: <post-image>
{ $values
    { "post" hashtable }
    { "post-image" image-gadget }
}
{ $description "" } ;

HELP: <search-failed>
{ $values
    { "label" label }
}
{ $description "" } ;

HELP: ?use-placeholder
{ $values
    { "disc" string }
    { "disc/placeholder" string }
}
{ $description "" } ;

HELP: add-padding
{ $values
    { "gadget" gadget }
    { "border" border }
}
{ $description "" } ;

HELP: e6-custom.
{ $values
    { "post" hashtable }
}
{ $description "Search e6 with a custom e621.net/post.json object and embed it into the current output stream." } ;

HELP: e6.
{ $values
    { "tags" string }
}
{ $description "Looks up a post and embeds it into the current output stream." } ;

HELP: get-description
{ $values
    { "post" hashtable }
    { "descr/placeholder" string }
}
{ $description "Returns the post description or a placeholder string." } ;

HELP: handle-error
{ $values
    { "tags" string } { "error" error }
    { "?gadget" label }
}
{ $description "Rethrows error if it doesn't mach download-failed or bounds-error." } ;

HELP: layout-post
{ $values
    { "gadgets" array }
    { "gadget" gadget }
}
{ $description "" } ;

HELP: make-post-gadgets
{ $values
    { "json" hashtable }
    { "gadgets" array }
}
{ $description "" } ;

HELP: wrap-description
{ $values
    { "string" string }
    { "string'" string }
}
{ $description "" } ;

ARTICLE: "e621.ui" "e621.ui"
{ $vocab-link "e621.ui" }
;

ABOUT: "e621.ui"
