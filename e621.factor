! Copyright (C) 2022 Quan Zillan
! See http://factorcode.org/license.txt for BSD license.
USING: accessors assocs calendar combinators combinators.smart
e621.json formatting http images.http images.viewer json.http
kernel math math.parser namespaces sequences splitting strings
threads ui.gadgets ui.gadgets.borders ui.gadgets.labels
ui.gadgets.packs ui.gadgets.panes ;
IN: e621

SYMBOL: default-score
25 default-score set-global

SYMBOL: default-order
"random" default-order set-global

SYMBOL: default-blacklist
" -cub -gore -scat -shota -loli -snuff -type:gif -type:swf -type:webm" default-blacklist set-global

CONSTANT: post-url "https://e621.net/posts.json?tags=%s+score:>=%d+order:%s&limit=%d"

: format-url ( tags score order limit -- url )
    post-url sprintf ;

: apply-blacklist ( tags -- tags+blacklist )
    default-blacklist get-global append ;

: tags>url-param ( tags -- search-tags )
    " " "+" replace ;

TUPLE: e621.net/posts.json
    { tags          string  initial: ""       }
    { minimum-score integer initial: 20       }
    { order         string  initial: "random" }
    { limit         integer initial: 1        }
;

: <e621.net/posts.json> ( -- url-tuple )
    \ e621.net/posts.json new ;

: fetch-sample-image ( post -- image )
    post-sample-url load-http-image ;

: wait-for-rate-limit ( -- )
    1 seconds sleep ;

: fetch-multiple-sample-images ( posts -- images )
    [ wait-for-rate-limit fetch-sample-image ] map ;

: >search-url ( tuple -- url )
    {
        [ tags>> apply-blacklist tags>url-param ]
        [ minimum-score>> ]
        [ order>> ]
        [ limit>> ]
    } cleave format-url ;

: search-e621-posts ( tuple -- posts )
    >search-url http-get-json get-posts nip ;

:: get-random-e621-post ( tags -- post )
    <e621.net/posts.json>
        tags                      >>tags
        default-score get-global  >>minimum-score
        default-order get-global  >>order
        1                         >>limit
    search-e621-posts first ;

: e6 ( tags -- image )
    get-random-e621-post fetch-sample-image ;

: e6* ( posts.json -- images )
    search-e621-posts fetch-multiple-sample-images ;
