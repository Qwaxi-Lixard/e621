# E621 for Factor

*Look up images on E621 without ever having to leave [Factor](https://factorcode.org/)'s listener!*

This vocabulary provides a basic set of words to search for images on e621.net. Additionally, the vocabulary e621.ui provides the `e6.` and `e6-custom.` words for embedding search results directly into the Factor listener.

# Installation

To install this vocabulary, simply clone the `e621` vocabulary directly into the `work` directory in your Factor root director.

# Screenshot
![](https://i.imgur.com/sBuDMom.png)