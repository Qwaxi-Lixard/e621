! Copyright (C) 2022 Quan Zillan
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays combinators combinators.smart
continuations e621 e621.json formatting http.client
images.viewer kernel math.parser sequences ui.gadgets
ui.gadgets.borders ui.gadgets.labels ui.gadgets.packs
ui.gadgets.panes wrap.strings ;
IN: e621.ui

<PRIVATE
: <filled-pile> ( -- filled-pile )
    <pile> 1 >>fill ;

: add-padding ( gadget -- border )
    { 5 5 } <border> ;

: wrap-description ( string -- string' )
    144 wrap-string ;

: ?use-placeholder ( disc -- disc/placeholder )
    dup empty? [ drop "< No Description >" ] when ;

: get-description ( post -- descr/placeholder )
    post-description ?use-placeholder wrap-description ;

: <post-id-label> ( post -- post-id-label )
    post-id number>string <label> add-padding ;

: <post-description-label> ( post -- post-description-label  )
    get-description <label> add-padding ;

: <post-image> ( post -- post-image )
    <image-gadget> add-padding ;

: layout-post ( gadgets -- gadget )
     <filled-pile> swap add-gadgets ;

: make-post-gadgets ( json -- gadgets )
    {
        [ <post-id-label> ]
        [ <post-description-label> ]
        [ fetch-sample-image <post-image> ]
    } cleave 3array ;

: <no-results-label> ( tags -- label )
    "No results found for %s" sprintf <label> ;

: <search-failed> ( -- label )
    "Search failed, try agian later" <label> ;

: handle-error ( tags error -- ?gadget )
    {
      { [ dup download-failed? ] [ 2drop <search-failed>   ] }
      { [ dup bounds-error?    ] [ drop <no-results-label> ] }
      [ nip rethrow ]
    } cond ;

: <post-gadget> ( post -- gadgets )
    [ make-post-gadgets layout-post ] [ handle-error ] recover ;
PRIVATE>

: e6. ( tags -- )
    get-random-e621-post <post-gadget> gadget. ;

: e6-custom. ( post -- )
    search-e621-posts
        [ wait-for-rate-limit <post-gadget> ] map
        [ gadget. ] each ;
