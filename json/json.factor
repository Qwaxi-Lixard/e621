! Copyright (C) 2022 Quan Zillan
! See http://factorcode.org/license.txt for BSD license.
USING: assocs ;
IN: e621.json

: get-posts ( json -- posts )
  "posts" of ;

: post-id ( post -- id )
  "id" of ;

: post-created-at ( post -- created-at )
  "created_at" of ;

: post-updated-at ( post -- updated-at )
  "updated_at" of ;

: post-file-info ( post -- file-info )
  "file" of ;

: post-extension ( file-info -- extension )
  "ext" of ;

: post-md5 ( file-info -- md5 )
  "md5" of ;

: post-size ( file-info -- size )
  "size" of ;

: post-preview-info ( post -- preview-info )
  "preview" of ;

: post-sample-info ( post -- sample-info )
  "sample" of ;

: post-url ( any-info -- url )
  "url" of ;

: post-width ( any-info -- width )
  "width" of ;

: post-height ( any-info -- height )
  "height" of ;

: post-score ( post -- score )
  "score" of ;

: post-tags ( post -- tags )
  "tags" of ;

: post-rating ( post -- rating )
  "rating" of ;

: post-fav-count ( post -- fav-count )
  "fav_count" of ;

: post-sources ( post -- sources )
  "source" of ;

: post-pools ( post -- pools )
  "pools" of ;

: post-relationships ( post -- relationships )
  "relationships" of ;

: post-description ( post -- description )
  "description" of ;

: post-file-url ( post -- file-url )
  post-file-info post-url ;

: post-preview-url ( post -- preview-url )
  post-preview-info post-url ;

: post-sample-url ( post -- sample-url )
  post-sample-info post-url ;
