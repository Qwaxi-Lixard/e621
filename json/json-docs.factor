! Copyright (C) 2022 Quan Zillan.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays hashtables help.markup help.syntax kernel math
strings urls ;
IN: e621.json

HELP: get-posts
{ $values
    { "json" hashtable }
    { "posts" array }
}
{ $description "" } ;

HELP: post-created-at
{ $values
    { "post" hashtable }
    { "created-at" string }
}
{ $description "" } ;

HELP: post-description
{ $values
    { "post" hashtable }
    { "description" string }
}
{ $description "" } ;

HELP: post-extension
{ $values
    { "file-info" hashtable }
    { "extension" string }
}
{ $description "" } ;

HELP: post-fav-count
{ $values
    { "post" hashtable }
    { "fav-count" fixnum }
}
{ $description "" } ;

HELP: post-file-info
{ $values
    { "post" hashtable }
    { "file-info" hashtable }
}
{ $description "" } ;

HELP: post-file-url
{ $values
    { "post" hashtable }
    { "file-url" string }
}
{ $description "" } ;

HELP: post-height
{ $values
    { "any-info" hashtable }
    { "height" fixnum }
}
{ $description "" } ;

HELP: post-id
{ $values
    { "post" hashtable }
    { "id" fixnum }
}
{ $description "" } ;

HELP: post-md5
{ $values
    { "file-info" hashtable }
    { "md5" string }
}
{ $description "" } ;

HELP: post-pools
{ $values
    { "post" hashtable }
    { "pools" array }
}
{ $description "" } ;

HELP: post-preview-info
{ $values
    { "post" hashtable }
    { "preview-info" hashtable }
}
{ $description "" } ;

HELP: post-preview-url
{ $values
    { "post" hashtable }
    { "preview-url" string }
}
{ $description "" } ;

HELP: post-rating
{ $values
    { "post" hashtable }
    { "rating" string }
}
{ $description "" } ;

HELP: post-relationships
{ $values
    { "post" hashtable }
    { "relationships" hashtable }
}
{ $description "" } ;

HELP: post-sample-info
{ $values
    { "post" hashtable }
    { "sample-info" hashtable }
}
{ $description "" } ;

HELP: post-sample-url
{ $values
    { "post" hashtable }
    { "sample-url" string }
}
{ $description "" } ;

HELP: post-score
{ $values
    { "post" hashtable }
    { "score" hashtable }
}
{ $description "" } ;

HELP: post-size
{ $values
    { "file-info" hashtable }
    { "size" fixnum }
}
{ $description "" } ;

HELP: post-sources
{ $values
    { "post" hashtable }
    { "sources" array }
}
{ $description "" } ;

HELP: post-tags
{ $values
    { "post" hashtable }
    { "tags" hashtable }
}
{ $description "" } ;

HELP: post-updated-at
{ $values
    { "post" hashtable }
    { "updated-at" string }
}
{ $description "" } ;

HELP: post-url
{ $values
    { "any-info" hashtable }
    { "url" string }
}
{ $description "" } ;

HELP: post-width
{ $values
    { "any-info" hashtable }
    { "width" fixnum }
}
{ $description "" } ;

ARTICLE: "e621.json" "e621.json"
{ $vocab-link "e621.json" }
"\nThis vocabulary contains an assortment of short cuts that make"
"accessing e621 post easier and read more naturally."
"\nRefer to the e621 API for further details on each section."
;

ABOUT: "e621.json"
